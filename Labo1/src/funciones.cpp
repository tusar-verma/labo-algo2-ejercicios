#include <vector>
#include "algobot.h"

using namespace std;

// Ejercicio 1
vector<int> quitar_repetidos(vector<int> s) {
    vector<int> res;
    bool encontro = false;
    for (int elem: s) {
        for (int resElem: res) {
            encontro = encontro || resElem == elem;
        }
        if (!encontro) res.push_back(elem);
        encontro = false;
    }

    return res;
}

set<int> pasarVectorASet(vector<int> v) {
    set<int> auxSet;
    for (int elem: v) {
        auxSet.insert(elem);
    }
    return auxSet;
}

// Ejercicio 2
vector<int> quitar_repetidos_v2(vector<int> s) {
    set<int> auxSet = pasarVectorASet(s);
    vector<int> res;

    for (int elem: auxSet) {
        res.push_back(elem);
    }

    return res;
}


// Ejercicio 3
bool mismos_elementos(vector<int> a, vector<int> b) {
    set<int> sa = pasarVectorASet(a);
    set<int> sb = pasarVectorASet(b);


    return sa == sb;
}

// Ejercicio 4
bool mismos_elementos_v2(vector<int> a, vector<int> b) {
    set<int> sa = pasarVectorASet(a);
    set<int> sb = pasarVectorASet(b);


    return sa == sb;
}

int cantidadApariciones(int elem, vector<int> s) {
    int contar = 0;
    for (int e: s) {
        if (e == elem) contar++;
    }
    return contar;
}

// Ejercicio 5
map<int, int> contar_apariciones(vector<int> s) {
    map<int, int> res;
    set<int> sv = pasarVectorASet(s);
    for (int elem: sv) {
        res.insert(make_pair(elem,cantidadApariciones(elem, s) ));
    }

    return res;
}

// Ejercicio 6
vector<int> filtrar_repetidos(vector<int> s) {
    map<int, int> cantApariciones = contar_apariciones(s);
    vector<int> res;
    for (pair<int, int> k: cantApariciones) {
        if (k.second == 1) res.push_back(k.first);
    }
    return res;
}

// Ejercicio 7
set<int> interseccion(set<int> a, set<int> b) {
    set<int> res;
    for (int e: a)  {
        if (b.count(e) == 1) res.insert(e);
    }
    return res;
}

// Ejercicio 8
map<int, set<int>> agrupar_por_unidades(vector<int> s) {
    map<int, set<int>> res;
    for (int elem: s) {
        res[elem % 10].insert(elem);
    }
    return res;
}

char traducirCaracter(vector<pair<char,char>> tr, char c) {
    char res = c;
    for (pair<char,char> elem: tr) {
        if (elem.first == c) res = elem.second;
    }
    return res;
}

// Ejercicio 9
vector<char> traducir(vector<pair<char, char>> tr, vector<char> str) {
    vector<char> res;
    for (char elem: str) {
        res.push_back(traducirCaracter(tr, elem));
    }

    return res;
}

bool todasLibretasCoinciden(set<LU> a, set<LU> b) {
    return a == b;
}
bool ningunaLibretaCoincide(set<LU> a, set<LU> b) {
    bool encontroAlgunaCoincidencia = false;
    for (LU u: a) {
       encontroAlgunaCoincidencia = encontroAlgunaCoincidencia || b.count(u) == 1;
    }

    return !encontroAlgunaCoincidencia;
}

// Ejercicio 10
bool integrantes_repetidos(vector<Mail> s) {
    bool res = true;

    for (Mail m: s) {
        for (Mail n: s) {
            res = res && ( todasLibretasCoinciden(n.libretas(), m.libretas()) ||
                           ningunaLibretaCoincide(n.libretas(), m.libretas())    );

        }
    }

    return !res;
}

// Ejercicio 11
map<set<LU>, Mail> entregas_finales(vector<Mail> s) {
    map<set<LU>, Mail> res;
    for (Mail mail: s) {
        if (res[mail.libretas()].fecha() <= mail.fecha() && mail.adjunto()) {
            res[mail.libretas()] = mail;
        }
    }

    return res;
}
