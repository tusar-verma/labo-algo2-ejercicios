#include "Lista.h"
#include <cassert>

Lista::Lista() : _size(0), _primero(nullptr), _ultimo(nullptr) {
}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    if (this->_size != 0) {
        Nodo* iter = this->_primero;
        Nodo* iterSig;
        while (iter != nullptr) {
            iterSig = iter->sig;
            delete iter;
            iter = iterSig;
        }
    }
}

void Lista::destruirNodos() {

    Nodo* iter = this->_primero;
    Nodo* iterAux;

    while(iter != nullptr) {
        iterAux = iter->sig;
        delete iter;
        iter = iterAux;
    }
    this->_size = 0;
}

void Lista::copiarNodos(const Lista & aCopiar) {
    Nodo* aux = aCopiar._primero;
    while(aux != nullptr) {
        agregarAtras(aux->valor);
        aux = aux->sig;
    }
}

Lista& Lista::operator=(const Lista& aCopiar) {
    this->destruirNodos();
    this->copiarNodos(aCopiar);
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    Nodo* nuevo = new Nodo(elem);
    nuevo->ant = nullptr;
    nuevo->sig = _primero;

    _primero = nuevo;
    if (_size == 0) {
        _ultimo = nuevo;
    }

    _size++;
}

void Lista::agregarAtras(const int& elem) {
    Nodo* nuevo = new Nodo(elem);
    nuevo->sig = nullptr;

    if (_size > 0) {
        nuevo->ant = _ultimo;
        _ultimo->sig = nuevo;
    } else{
        nuevo->ant = nullptr;
        _primero = nuevo;
    }
    _ultimo = nuevo;

    _size++;

}

void Lista::eliminar(Nat i) {

    Nodo* ant;
    Nodo* actual= _primero;
    for (int j = 0; j < i; j++) {
        ant = actual;
        actual = actual->sig;
    }
    if (actual == _primero) {
        _primero = _primero->sig;
    } else if (actual == _ultimo) {
        ant->sig = nullptr;
    } else {
        ant->sig = actual->sig;
        actual->sig->ant = ant;

    }
    _size--;
    delete actual;

}

int Lista::longitud() const {
    return _size;
}

const int& Lista::iesimo(Nat i) const {
    Nodo* actual= _primero;
    for (int j = 0; j < i; j++) {
        actual = actual->sig;
    }
    return actual->valor;
}

int& Lista::iesimo(Nat i) {
    Nodo* actual= _primero;
    for (int j = 0; j < i; j++) {
        actual = actual->sig;
    }
    return actual->valor;
}

void Lista::mostrar(ostream& o) {
    Nodo* iter = this->_primero;
    int count = 0;

    while (iter != nullptr) {
        o << "Nodo " << count << ": " << iter->valor << "; ";
        count++;
        iter = iter->sig;
    }
}
