#include "SistemaDeMensajes.h"

// Completar...
SistemaDeMensajes::SistemaDeMensajes() {
    for (int i = 0; i < 4; i++) {
        _conns[i] = nullptr;
    }
}

void SistemaDeMensajes::registrarJugador(int id, string ip) {
    desregistrarJugador(id);
    _conns[id] = new ConexionJugador(ip);
}

void SistemaDeMensajes::desregistrarJugador(int id) {
    delete _conns[id];
    _conns[id] = nullptr;
}

bool SistemaDeMensajes::registrado(int id) const {
    return _conns[id] != nullptr;
}

void SistemaDeMensajes::enviarMensaje(int id, string mensaje) {
    _conns[id]->enviarMensaje(mensaje);
}

string SistemaDeMensajes::ipJugador(int id) const {
    return  _conns[id]->ip();;
}


SistemaDeMensajes::Proxy* SistemaDeMensajes::obtenerProxy(int id) {
    Proxy* p = new Proxy(&_conns[id]);
    _proxies.push_back(p);
    return (p);
}

SistemaDeMensajes::Proxy::Proxy(ConexionJugador **conn) : _conn(conn) {}

void SistemaDeMensajes::Proxy::enviarMensaje(std::string msg) {
    (*_conn)->enviarMensaje(msg);
}

SistemaDeMensajes::~SistemaDeMensajes() {
    for (int i = 0; i < 4; i++) {
        delete _conns[i];
        _conns[i] = nullptr;
    }
    for (Proxy* p: _proxies) {
        delete p;
    }
}


