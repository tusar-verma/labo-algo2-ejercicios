#ifndef ALGO2_LABO_CLASE5_ALGORITMOS_H
#define ALGO2_LABO_CLASE5_ALGORITMOS_H

#include <utility>
#include <iterator>
#include <vector>

// Completar con las funciones del enunciado

template<class Contenedor>
typename Contenedor::value_type minimo(const Contenedor& c) {
    typename Contenedor::const_iterator min = c.begin();
    for (typename Contenedor::const_iterator it = c.begin(); it != c.end(); ++it) {
        if (*it < *min) {
            min = it;
        }
    }
    return *min;
}

template<class Contenedor>
typename Contenedor::value_type promedio(const Contenedor& c) {
    typename Contenedor::value_type res = 0;
    int count = 0;
    for (typename Contenedor::value_type x : c) {
        count++;
        res += x;
    }
    return (res / count);
}


template<class Iterator>
typename Iterator::value_type minimoIter(const Iterator& desde, const Iterator& hasta) {
    Iterator min = desde;
    for (Iterator i = desde; i != hasta; ++i) {
        if (*i < *min) {
            min = i;
        }
    }
    return *min;
}

template<class Iterator>
typename Iterator::value_type promedioIter(const Iterator& desde, const Iterator& hasta) {
    typename Iterator::value_type res = 0;
    int count = 0;
    for (Iterator it = desde; it != hasta; ++it) {
        res += *it;
        count++;
    }
    return (res / count);
}

template<class Contenedor>
void filtrar(Contenedor &c, const typename Contenedor::value_type& elem) {
    typename Contenedor::const_iterator it = c.begin();
    while (it != c.end()) {
        if (*it == elem) {
            it = c.erase(it);
        } else {
            ++it;
        }
    }
}

template<class Contenedor>
bool ordenado(Contenedor &c) {
    typename Contenedor::const_iterator it = c.begin();
    typename Contenedor::const_iterator itSig = ++c.begin();
    bool res = true;

    while (itSig != c.end()) {
        if (*it > *itSig) {
            res = false;
        }
        ++it;
        ++itSig;
    }

    return res;
}


template<class Contenedor>
std::pair<Contenedor, Contenedor> split(const Contenedor & c, const typename Contenedor::value_type& elem){
    Contenedor chicos;
    Contenedor grandes;

    for (typename Contenedor::const_iterator it = c.begin(); it != c.end(); ++it) {
        if (*it < elem) {
            chicos.insert(chicos.end(), *it);
        } else {
            grandes.insert(grandes.end(), *it);

        }
    }
    return std::make_pair(chicos, grandes);
}

template <class Contenedor>
void merge(const Contenedor& c1, const Contenedor & c2, Contenedor & res) {
    typename Contenedor::const_iterator it1 = c1.begin();
    typename Contenedor::const_iterator it2 = c2.begin();

    while (it1 != c1.end() || it2 != c2.end()) {
        if (it1 == c1.end() && it2 != c2.end()) {
            res.insert(res.end(), *it2);
            ++it2;
        } else if (it1 != c1.end() && it2 == c2.end()) {
            res.insert(res.end(), *it1);
            ++it1;
        } else {
            if (*it1 < *it2) {
                res.insert(res.end(), *it1);
                ++it1;
            } else {
                res.insert(res.end(), *it2);
                ++it2;
            }
        }


    }
}

#endif //ALGO2_LABO_CLASE5_ALGORITMOS_H
