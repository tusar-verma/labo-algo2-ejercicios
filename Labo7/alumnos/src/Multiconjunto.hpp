#ifndef MULTICONJUNTO
#define MULTICONJUNTO

#include "Diccionario.hpp"

template<class T>
class Multiconjunto {
public:
    Multiconjunto();
    void agregar(T x);
    int ocurrencias(T x) const;
    bool operator<=(Multiconjunto<T> otro) const;

private:
    Diccionario<T, int> multiconjunto;

};

template<class T>
Multiconjunto<T>::Multiconjunto() {
}

template<class T>
bool Multiconjunto<T>::operator<=(Multiconjunto<T> otro) const {
    std::vector<T> claves = multiconjunto.claves();
    for (int i = 0; i < claves.size(); i++) {
        if (ocurrencias(claves[i]) > otro.ocurrencias(claves[i])) {
            return false;
        }
    }
    return true;
}

template<class T>
void Multiconjunto<T>::agregar(T x) {
    if (multiconjunto.def(x)) {
        multiconjunto.definir(x, multiconjunto.obtener(x) + 1);
    } else {
        multiconjunto.definir(x, 1);
    }

}

template<class T>
int Multiconjunto<T>::ocurrencias(T x) const {
    if (multiconjunto.def(x)) {
        return multiconjunto.obtener(x);
    }
    return 0;
}



#endif