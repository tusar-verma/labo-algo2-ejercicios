
template<class T>
ColaPrior<T>::ColaPrior() {
	_size = 0;
}

template<class T>
int ColaPrior<T>::tam() const {
	return _size;
}

template<class T>
void ColaPrior<T>::encolar(const T& elem) {
    elementos.push_back(elem);
    int indElem = elementos.size()-1;
    int indPadre = (indElem-1)/2;

    while (indElem != 0 && elem > elementos[indPadre]) {
        T aux = elementos[indPadre];
        elementos[indPadre] = elem;
        elementos[indElem] = aux;

        indElem = indPadre;
        indPadre = (indElem-1)/2;
    }
    _size ++;

}

template<class T>
const T& ColaPrior<T>::proximo() const {
	return elementos[0];
}

template<class T>
void ColaPrior<T>::desencolar() {
	elementos[0] = elementos[elementos.size() -1];
    elementos.pop_back();
    _size--;
    int indElem = 0;
    int indIzq, indDer;

    bool sifteo = true;

    while ( sifteo ) {
        indIzq = 2*indElem +1;
        indDer = 2*indElem +2;

        if (indIzq < elementos.size() && indDer < elementos.size()) {
            if (elementos[indIzq] > elementos[indDer] && elementos[indIzq] > elementos[indElem]) {
                sift(indElem, indIzq);
            } else if (elementos[indIzq] <= elementos[indDer] && elementos[indDer] > elementos[indElem]) {
                sift(indElem, indDer);
            } else {
                sifteo = false;
            }
        } else if (indIzq < elementos.size() && indDer >= elementos.size()) {
            if (elementos[indIzq] > elementos[indElem]) {
                sift(indElem, indIzq);
            } else {
                sifteo = false;
            }
        } else if (indDer < elementos.size() && indIzq >= elementos.size()) {
            if (elementos[indDer] > elementos[indElem]) {
                sift(indElem, indDer);
            } else {
                sifteo = false;
            }
        } else{
            sifteo = false;
        }
    }
}
template<class T>
void ColaPrior<T>::sift(int indElem, int indSift) {
    T aux = elementos[indElem];
    elementos[indElem] = elementos[indSift];
    elementos[indSift] = aux;
    indElem = indSift;
}

template<class T>
bool ColaPrior<T>::NoEsHoja(int indElem) {
    return (2*indElem +1 < elementos.size() || 2*indElem +2 < elementos.size());
}

template<class T>
ColaPrior<T>::ColaPrior(const vector<T>& elems) {
	// COMPLETAR
}

