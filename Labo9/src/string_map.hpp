template <typename T>
string_map<T>::string_map(){
    _size = 0;
    raiz = new Nodo();
}


template<typename T>
string_map<T>::Nodo::Nodo() {
    definicion = nullptr;
    for (int i = 0; i < 255; i++) {
        siguientes.push_back(nullptr);
    }
}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
    eliminarTodo(raiz);
    raiz = new Nodo();
    _size = d._size;

    copiarNodo(raiz, d.raiz);
    return *this;
}


template<typename T>
void string_map<T>::copiarNodo(string_map::Nodo* dest, const string_map::Nodo* copiar) {
    if (copiar->definicion != nullptr) {
        dest->definicion = new T(*copiar->definicion);

    }
    for (int i = 0; i < 255; i++) {
        if (copiar->siguientes[i] != nullptr) {
            dest->siguientes[i] = new Nodo();
            copiarNodo(dest->siguientes[i],copiar->siguientes[i] );
        }
    }
}


template <typename T>
string_map<T>::~string_map(){
    eliminarTodo(raiz);
    raiz = nullptr;
    _size = 0;
}


template<typename T>
void string_map<T>::eliminarTodo(Nodo* n) {

    for (int i = 0; i < 255; i++) {
        if (n->siguientes[i] != nullptr) {
            if (n->siguientes[i]->definicion != nullptr) {
                delete n->siguientes[i]->definicion;
                n->siguientes[i]->definicion = nullptr;
            }
            eliminarTodo(n->siguientes[i]);
            n->siguientes[i] = nullptr;
        }
    }
    delete n;
}

template <typename T>
T& string_map<T>::operator[](const string& clave){
}


template<typename T>
void string_map<T>::insert(const pair<string, T> & claveValor) {
    Nodo* aux = raiz;
    int indClave = 0;
    string clave = claveValor.first;
    T* valor = new T(claveValor.second);

    while (indClave < clave.size()) {
        if (aux->siguientes[int(clave[indClave])] == nullptr){
            aux->siguientes[int(clave[indClave])] = new Nodo();
        }
        aux = aux->siguientes[int(clave[indClave])];
        indClave++;
    }
    if (aux->definicion != nullptr) {
        delete aux->definicion;
    }
    aux->definicion = valor;
    _size++;
}

template<typename T>
bool string_map<T>::definido(const string &clave) const {
    if (raiz == nullptr) {
        return false;
    }
    Nodo* aux = raiz;
    int indClave = 0;

    while (indClave < clave.size()) {
        if (aux->siguientes[int(clave[indClave])] != nullptr){
            aux = aux->siguientes[int(clave[indClave])];
            indClave++;
        } else {
            return false;
        }
    }

    return aux->definicion != nullptr;

}


template <typename T>
int string_map<T>::count(const string& clave) const{
    if (definido(clave)) {
        return 1;
    }
    return 0;
}

//pre: la clave esta definida
template <typename T>
const T& string_map<T>::at(const string& clave) const {

    return at(clave);
}

template <typename T>
T& string_map<T>::at(const string& clave) {

    Nodo* aux = raiz;
    int indClave = 0;

    while (indClave < clave.size()) {
        aux = aux->siguientes[int(clave[indClave])];
        indClave++;
    }
    return *aux->definicion;
}


//pre: la clave esta definida
template <typename T>
void string_map<T>::erase(const string& clave) {

    Nodo* aux = raiz;
    Nodo* ultimoNoBorrar = raiz;
    int indClave = 0;
    int indClaveUlt = 0;

    while (indClave < clave.size()) {

        aux = aux->siguientes[int(clave[indClave])];
        indClave++;

        if (tieneMasDeUnHijo(aux) || aux->definicion != nullptr) {
            ultimoNoBorrar = aux;
            indClaveUlt = indClave;
        }
    }

    delete aux->definicion;
    aux->definicion = nullptr;
    _size--;

    if (ultimoNoBorrar != aux) {

        borrarNodo(ultimoNoBorrar->siguientes[int(clave[indClaveUlt])]);
        ultimoNoBorrar->siguientes[int(clave[indClaveUlt])] = nullptr;
    }
}

template<typename T>
void string_map<T>::borrarNodo(string_map<T>::Nodo* n) {

    for (int i = 0; i < 255; i++) {
        if (n->siguientes[i] != nullptr) {
            borrarNodo(n->siguientes[i]);
        }
    }

    delete n;

}

template<typename T>
bool string_map<T>::tieneMasDeUnHijo(string_map::Nodo * n) {
    int count = 0;
    for (int i = 0; i < 255; i++) {
        if (n->siguientes[i] != nullptr) {
            count++;
            if (count > 1) {
                return true;
            }
        }
    }
    return false;
}

template <typename T>
int string_map<T>::size() const{
    return _size;
}

template <typename T>
bool string_map<T>::empty() const{
    for (int i = 0; i < 255; i++) {
        if (raiz->siguientes[i] != nullptr) {
            return false;
        }
    }
    return true;
}