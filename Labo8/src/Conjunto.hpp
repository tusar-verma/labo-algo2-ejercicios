

template<class T>
Conjunto<T>::Nodo::Nodo(const T &v) {
    valor = v;
    izq = nullptr;
    der = nullptr;
}

template <class T>
Conjunto<T>::Conjunto() {
    // Completar
    _raiz = nullptr;
    _size = 0;
}

template <class T>
Conjunto<T>::~Conjunto() {
    /*vector<Nodo*> subArboles;
    subArboles.push_back(_raiz);
    Nodo* aux;

    while (!subArboles.empty()) {
        aux = subArboles[subArboles.size() - 1];
        subArboles.pop_back();

        if (aux->izq != nullptr) {
            subArboles.push_back(aux->izq);
        }
        if (aux->der != nullptr) {
            subArboles.push_back(aux->der);
        }

        delete aux;
    }*/

    while(_raiz != nullptr) {
        remover(_raiz->valor);
    }
}

// devuelve el padre que lo deberia tener (si no esta) o el padre q lo tiene (si esta)
// pre: al menos hay un elemento en el arbol
template<class T>
typename Conjunto<T>::Nodo* Conjunto<T>::padreDe(const T &clave) const {

    Nodo* aux = _raiz;
    Nodo* auxPadre = nullptr;

    while (aux != nullptr && aux->valor != clave) {
        auxPadre = aux;
        if (aux->valor > clave) {
            aux = aux->izq;
        } else {
            aux = aux->der;
        }
    }

    return auxPadre;
}

//pre: hay al menos un elemento
template<class T>
typename Conjunto<T>::Nodo* Conjunto<T>::buscar(const T &clave) const {

    Nodo* aux = _raiz;
    bool res = false;

    while (aux != nullptr && !res) {
        if (aux->valor != clave) {
            if (aux->valor > clave) {
                aux = aux->izq;
            } else {
                aux = aux->der;
            }
        } else {
            res = true;
        }
    }

    if (res) {
        return aux;
    }
    return nullptr;
}



template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    if (_raiz == nullptr) {
        return false;
    } else if (_raiz->valor == clave){
        return true;
    } else {
        return (buscar(clave) != nullptr);
    }
}

template <class T>
void Conjunto<T>::insertar(const T& clave) {
    if (_raiz == nullptr) {
        _raiz = new Nodo(clave);
        _size++;
    } else {
        if (_raiz->valor != clave) {
            Nodo* p = padreDe(clave);
            if (p->valor > clave && p->izq == nullptr) {
                p->izq = new Nodo(clave);
                _size++;
            } else if (p->valor < clave && p->der == nullptr){
                p->der = new Nodo(clave);
                _size++;
            }
        }
    }
}
//caso borrar raiz falta
template <class T>
void Conjunto<T>::remover(const T& elem) {
    if (pertenece(elem)) {
        Nodo* aBorrar = buscar(elem);

        if (aBorrar->izq == nullptr && aBorrar->der == nullptr) {
            //caso sin hijos
            Nodo* padreABorrar = padreDe(elem);

            if (padreABorrar == nullptr) {
                _raiz = nullptr;
            } else {
                if (padreABorrar->der == aBorrar) {
                    padreABorrar->der = nullptr;
                } else {
                    padreABorrar->izq = nullptr;
                }
            }

            delete aBorrar;

        } else if (aBorrar->izq != nullptr && aBorrar->der != nullptr) {
            //caso tiene 2 hijos
            T sucValor = siguiente((elem));
            Nodo* sucInmediato = buscar(sucValor);
            Nodo* padreSuc = padreDe(sucValor);

            aBorrar->valor = sucInmediato->valor;

            if (sucInmediato->der != nullptr) {
                borrarNodoConUnHIjo(sucInmediato);
            } else {
                if (padreSuc->izq == sucInmediato) {
                    padreSuc->izq = nullptr;
                } else {
                    padreSuc->der = nullptr;
                }
                delete sucInmediato;
            }

        } else {
            //caso un solo hijo
            borrarNodoConUnHIjo(aBorrar);
        }

        _size--;
    }
}

//pre: el nodo a borrar solo tiene un hijo.
template<class T>
void Conjunto<T>::borrarNodoConUnHIjo(Conjunto::Nodo *aBorrar) {

    if (aBorrar->der != nullptr) {
        Nodo* aux = aBorrar->der;
        aBorrar->valor = aux->valor;
        aBorrar->der = aux->der;
        aBorrar->izq = aux->izq;

        delete aux;

    } else {
        Nodo* aux = aBorrar->izq;
        aBorrar->valor = aux->valor;
        aBorrar->der = aux->der;
        aBorrar->izq = aux->izq;

        delete aux;
    }
}

//pre: el nodo de clave esta en el conjunto y tiene un siguiente
template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
    Nodo* aux = buscar(clave);

    if (aux->der == nullptr) {
        Nodo* posibleSiguiente = _raiz;
        Nodo* recorredorAbb = _raiz;

        while (recorredorAbb != aux) {
            if (aux->valor < recorredorAbb->valor) {
                posibleSiguiente = recorredorAbb;
                recorredorAbb = recorredorAbb->izq;
            } else {
                recorredorAbb = recorredorAbb->der;
            }
        }

        aux = posibleSiguiente;

    } else {
        aux = aux->der;

        while(aux->izq != nullptr) {
            aux = aux->izq;
        }

    }

    return aux->valor;

}

//pre: el conj no es vacio
template <class T>
const T& Conjunto<T>::minimo() const {
    Nodo* aux = _raiz;

    while (aux->izq != nullptr) {
        aux = aux->izq;
    }
    return aux->valor;
}

//pre: el conj no es vacio
template <class T>
const T& Conjunto<T>::maximo() const {
    Nodo* aux = _raiz;

    while (aux->der != nullptr) {
        aux = aux->der;
    }
    return aux->valor;
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return _size;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}

