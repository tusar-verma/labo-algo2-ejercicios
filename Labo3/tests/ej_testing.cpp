#include "gtest-1.8.1/gtest.h"
#include "../src/Libreta.h"
#include "../src/Truco.h"
#include <map>
#include <cmath>

using namespace std;

// Ejercicio 4
TEST(Aritmetica, suma) {
    int valorComputado = 15+7;
    int valorEsperado = 22;

    EXPECT_EQ(valorComputado, valorEsperado);

}

// Ejercicio 5
TEST(Aritmetica, potencia) {
    EXPECT_EQ(pow(10,2),  100);
}


// Ejercicios 6..9
TEST(Aritmetica, potencia_general) {
    for (int i = -5; i<= 5; i++) {
        EXPECT_EQ(pow(i, 2), i * i);
    }
}

TEST(Diccionario, obtener) {
    map<int, int> m;
    m.insert(make_pair(0,0));

    EXPECT_EQ(m[0], 0);
}
TEST(Diccionario, definir) {
    map<int, int> m;
    EXPECT_EQ(m.count(0), 0);
    m.insert(make_pair(0,0));
    EXPECT_EQ(m.count(0),1);
}

TEST(Truco, inicio) {
    Truco juego = Truco();
    EXPECT_EQ(juego.puntaje_j1(), 0);
    EXPECT_EQ(juego.puntaje_j2(), 0);
}

TEST(Truco, buenas) {
    Truco t;
    EXPECT_FALSE(t.buenas(1));

    for (int i = 0; i < 15; i++) {
        t.sumar_punto(1);
    }
    EXPECT_FALSE(t.buenas(1));
    t.sumar_punto(1);
    EXPECT_TRUE(t.buenas(1));
    t.sumar_punto(1);
    t.sumar_punto(1);
    EXPECT_TRUE(t.buenas(1));

}