//
// Created by PC on 13/4/2023.
//

#ifndef SOLUCION_FUNCIONES_H
#define SOLUCION_FUNCIONES_H

typedef int Anio;

bool esBisiesto(Anio anio);
int diasEnMes(int anio, int mes);

#endif //SOLUCION_FUNCIONES_H
