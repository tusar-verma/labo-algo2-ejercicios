#include <utility>

using namespace std;

// Taller 1 - Juego

using Pos = pair<int, int>;
using uint = unsigned int;

char ARRIBA = '^';
char ABAJO = 'v';
char DERECHA = '<';
char IZQUIERDA = '>';

class Juego {
  // Completar
    public:
        Juego(uint casilleros, Pos pos_inicial);
        Pos posicion_jugador();
        uint turno_actual();
        void mover_jugador(char dir);
        void ingerir_pocion(uint movimientos, uint turnos);
    private:
        Pos _pos_actual;
        uint _turno;
        uint _tamanio_tablero;
        uint _cant_mov_turno_actual;
        vector<pair<uint, uint>> _pociones_ingeridas;
        void _reducir_turnos_pociones();
        uint _movimientos_nuevo_turno();
};

Juego::Juego(uint casilleros, Pos pos_inicial) : _tamanio_tablero(casilleros), _pos_actual(pos_inicial) {
    _turno = 0;
    _cant_mov_turno_actual = _movimientos_nuevo_turno();
}

uint Juego::_movimientos_nuevo_turno() {
    uint res = 0;
    for (pair<uint,uint> p: _pociones_ingeridas) {
        res += p.first;
    }
    if (res == 0) res = 1;
    return res;
}

void Juego::ingerir_pocion(uint movimientos, uint turnos) {
    _pociones_ingeridas.push_back(make_pair(movimientos, turnos));
    _cant_mov_turno_actual = _movimientos_nuevo_turno();
}

Pos Juego::posicion_jugador() {
    return _pos_actual;
}

uint Juego::turno_actual() {
    return _turno;
}


void Juego::mover_jugador(char dir) {
    if (dir == IZQUIERDA) {
        if (_pos_actual.second > 0) {
            _pos_actual.second--;
        }
    } else if (dir == DERECHA) {
        if (_pos_actual.second < _tamanio_tablero - 2) {
            _pos_actual.second++;
        }
    } else if (dir == ARRIBA) {
        if(_pos_actual.first > 0) {
            _pos_actual.first--;
        }
    } else if (dir == ABAJO){
        if (_pos_actual.first < _tamanio_tablero - 2) {
            _pos_actual.first++;
        }
    }
    _cant_mov_turno_actual--;
    if (_cant_mov_turno_actual == 0) {
        _turno++;
        _reducir_turnos_pociones();
        _cant_mov_turno_actual = _movimientos_nuevo_turno();
    }
}

void Juego::_reducir_turnos_pociones() {
    vector<pair<uint, uint>> res;
    for (pair<uint, uint> p: _pociones_ingeridas) {
        p.second--;
        if (p.second != 0) {
            res.push_back(p);
        }
    }
    _pociones_ingeridas = res;
}
