#include <iostream>
#include <list>

using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
  public:
    // Completar declaraciones funciones
    #if EJ >= 9 // Para ejercicio 9
    bool operator==(Fecha o);
    #endif
    Fecha(int mes, int dia);
    int mes();
    int dia();
    void incrementar_dia();

  private:
    //Completar miembros internos
    int _mes;
    int _dia;
};
Fecha::Fecha(int mes, int dia) : _mes(mes), _dia(dia){}

int Fecha::mes() {
    return _mes;
}

int Fecha::dia() {
    return _dia;
}

ostream& operator<<(ostream& os, Fecha f) {
    os << f.dia() << "/" << f.mes();
    return os;
}

#if EJ >= 9
bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia();
    // Completar iguadad (ej 9)
    igual_dia = igual_dia && this->mes() == o.mes();
    return igual_dia;
}
#endif

void Fecha::incrementar_dia() {
    if (this->dia() == dias_en_mes(this->mes())) {
        if (this->mes() == 12) {
            this->_mes = 1;
        } else{
            this->_mes++;
        }
        this->_dia = 1;
    } else {
        this->_dia++;
    }

}

// Ejercicio 11, 12

// Clase Horario
class Horario{
    public:
        Horario(uint hora, uint min);
        uint hora();
        uint min();
        bool operator<(Horario h);
    private:
        uint _hora;
        uint _min;
};

Horario::Horario(uint hora, uint min) : _hora(hora), _min(min) {}

uint Horario::hora() {
    return _hora;
}

uint Horario::min() {
    return _min;
}

ostream& operator<<(ostream& os, Horario h) {
    os << h.hora() << ":" << h.min();
    return os;
}

bool Horario::operator<(Horario h) {
    if (this->hora() < h.hora()) {
        return true;
    } else if (this->hora() > h.hora()) {
        return false;
    } else  {
        return (this->min() < h.min());
    }
}
// Ejercicio 13

// Clase Recordatorio
class Recordatorio{
    public:
        Recordatorio(Fecha f, Horario h, string s);
        Fecha fecha();
        Horario horario();
        string mensaje();
    private:
        Fecha _f;
        Horario _h;
        string _mensaje;
};

Recordatorio::Recordatorio(Fecha f, Horario h, std::string s) : _f(f), _h(h), _mensaje(s) {}

Horario Recordatorio::horario() {
    return _h;
}

Fecha Recordatorio::fecha() {
    return _f;
}

string Recordatorio::mensaje() {
    return _mensaje;
}

ostream& operator<<(ostream& os, Recordatorio r) {
    os << r.mensaje() << " @ " << r.fecha() << " " << r.horario();
    return os;
}

// Ejercicio 14

class Agenda{
    public:
        Agenda(Fecha fecha_inicial);
        void agregar_recordatorio(Recordatorio red);
        void incrementar_dia();
        list<Recordatorio> recordatorios_de_hoy();
        Fecha hoy();
    private:
        list<Recordatorio> _recordatorios;
        Fecha _fecha_actual;
};
Agenda::Agenda(Fecha fecha_inicial) : _fecha_actual(fecha_inicial) {}

Fecha Agenda::hoy() {
    return _fecha_actual;
}

void Agenda::incrementar_dia() {
    _fecha_actual.incrementar_dia();
}

void Agenda::agregar_recordatorio(Recordatorio red) {
    _recordatorios.push_back(red);
}

bool comparacion_por_horario ( Recordatorio r1, Recordatorio r2) {
    return (r1.horario() < r2.horario());
}

list <Recordatorio> Agenda::recordatorios_de_hoy() {
    list<Recordatorio> res;
    for (Recordatorio r: _recordatorios) {
        if (r.fecha() == _fecha_actual){
            res.push_back(r);
        }
    }
    res.sort(comparacion_por_horario);
    return res;
}

ostream& operator<<(ostream& os, Agenda a) {
    os << a.hoy() << "\n" << "=====" << "\n";
    list<Recordatorio> rec_de_hoy = a.recordatorios_de_hoy();
    for (Recordatorio r: rec_de_hoy) {
        os << r.mensaje() << " @ " << r.fecha() << " " << r.horario() << "\n";
    }
    return os;
}

// Clase Agenda

